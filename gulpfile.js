'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass');


// Compile Sass & Inject Into Browser
gulp.task('sass', function () {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("src/css"))
        .pipe(browserSync.stream());
});

// Move JS Files to src/js
gulp.task('js', function () {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest('src/js'))
        .pipe(browserSync.stream());
});

// Watch Sass & Serve
gulp.task('serve', ['sass'], function () {

    browserSync.init({
        server: "./src"
    });

    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

// Move Fonts to src/fonts
gulp.task('fonts', function () {
    return gulp.src('node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('src/fonts'))
});

// Move Font Awesome CSS to src/css
gulp.task('fa', function () {
    return gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
        .pipe(gulp.dest('src/css'))
});

// Create empty directory images
gulp.task('images', function () {
    return gulp.src('*.*', {read: false})
        .pipe(gulp.dest('src/images'));
});

// Move html to build
gulp.task('html', function () {
    return gulp.src('src/*.html')
        .pipe(gulp.dest('build/'))
});

// Move html to build
gulp.task('html:build', function () {
    return gulp.src('src/*.html')
        .pipe(gulp.dest('build/'))
});

// Move js to build
gulp.task('js:build', function () {
    return gulp.src('src/js/*.js')
        .pipe(gulp.dest('build/js'))
});

// Move style to build
gulp.task('style:build', function () {
    return gulp.src('src/css/*.css')
        .pipe(gulp.dest('build/css'))
});

// Move fonts to build
gulp.task('fonts:build', function () {
    return gulp.src('src/fonts/*.*')
        .pipe(gulp.dest('build/fonts'))
});

// Move imagess to build
gulp.task('images:build', function () {
    return gulp.src('src/images/*.*')
        .pipe(gulp.dest('build/images'))
});


// Make build
gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'images:build',
    'fonts:build'
]);



gulp.task('default', ['js', 'serve', 'fa', 'fonts', 'images', 'build']);